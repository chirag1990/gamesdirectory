//
//  GamesDetailsViewController.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 18/02/2021.
//

import UIKit
import SDWebImage
import TTTAttributedLabel
import AVKit

class GamesDetailsViewController : UIViewController {
    
    var gameName : String = ""
    var gameSlug : String = ""
    var urlToLoad : String = ""
    let kReadMoreText =  "...ReadMore"
    let kReadLessText =  "...ReadLess"
    let kCharacterBeforReadMore = 500
    var strFull = ""
    var gameDetails: GameDetails?
    
    @IBOutlet weak var platformLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var trophyCollectionView: UICollectionView!
    @IBOutlet weak var gameCoverImageView: UIImageView!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var gameDescLabel: TTTAttributedLabel!
    
    @IBOutlet weak var coll_height: NSLayoutConstraint!
    @IBOutlet weak var coll_media: UICollectionView!
    
    @IBOutlet weak var trophy_height: NSLayoutConstraint!
    @IBOutlet weak var img_big: UIImageView!
    @IBOutlet weak var bg_imgView: UIView!
    
    
    override func viewDidLoad() {
        self.title = gameName
        trophyCollectionView.delegate = self
        trophyCollectionView.dataSource = self
        
        trophyCollectionView.register(TrophyCollectionViewCell.nib(),
                                      forCellWithReuseIdentifier: "trophyCell")
        trophyCollectionView.register(NoTrophiesCollectionViewCell.nib(),
                                      forCellWithReuseIdentifier: "noTrophyCell")
        
        getGameDetails()
    }
    
    func getGameDetails() {
        
        if NetworkConnection.shared.isConnectedToNetwork() {
            self.hudProggess(view, Show: true)
            urlToLoad = Constants.Services.BASE_URL + "/" + gameSlug
            
            GameDetailsService.shared.getGamesDetails(url: urlToLoad)
            GameDetailsService.shared.completionHandler { (gameDatas, status, message) in
                self.hudProggess(self.view, Show: false)
                if status {
                    guard let gameDetails = gameDatas else { return }
                    self.gameDetails = gameDetails.game
                    self.configureView(with: gameDetails)
                    self.trophyCollectionView.reloadData()
                    self.coll_media.reloadData()
                    let when = DispatchTime.now() + 0.5
                    
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        
                        self.coll_height.constant = self.coll_media.contentSize.height
                        self.trophy_height.constant = self.trophyCollectionView.contentSize.height
                    }
                    
                }
            }
        }
    }
    
    func configureView(with gameDetails : GamesDetailsModel) {
        let data = gameDetails.game
        let imageURL = data.covers.service_url
        self.gameCoverImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: [], context: nil)
        
        var txt = ""
        for obj in data.platforms {
            
            if txt == "" {
                txt = "\(obj)"
            }
            else {
                txt = txt + ", \(obj)"
            }
        }
        
        platformLabel.text = txt + ", " + "\(data.publishers.first?.label ?? "")" + ",\n" + "\(data.genres.first?.value.firstUppercased ?? "")"
        
        
        let currrenySymbol = getSymbol(forCurrencyCode: data.skus.first?.currency ?? "")
        let newPrice = Double(data.skus.first!.price) / 100.00
        priceLabel.text = currrenySymbol! + "\(Double(newPrice))"
        
        publisherLabel.text = data.publishers.first?.label.firstCapitalized
        genreLabel.text = data.genres.first?.value.firstCapitalized
        
        publisherLabel.numberOfLines = 0
        publisherLabel.sizeToFit()
        
        genreLabel.numberOfLines = 0
        genreLabel.sizeToFit()
        
        let dateString = data.releaseDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        let releaseDate = dateFormatter.date(from: dateString) ?? Date()
        
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = "EEEE, MMM dd, yyyy"
        let releaseDateString = newDateFormatter.string(from: releaseDate)
        releaseDateLabel.text = releaseDateString
        
        let newGameDesc = removeHtmlTags(with: data.gameDescription)
        strFull = newGameDesc
        
        gameDescLabel.showTextOnTTTAttributeLable(str: strFull, readMoreText: kReadMoreText, readLessText: kReadLessText, font: UIFont.init(name: "Helvetica-Bold", size: 24.0)!, charatersBeforeReadMore: kCharacterBeforReadMore, activeLinkColor: UIColor.blue, isReadMoreTapped: false, isReadLessTapped: false)
        gameDescLabel.delegate = self
    }
    
    func readMore(readMore: Bool) {
        gameDescLabel.showTextOnTTTAttributeLable(str: strFull, readMoreText: kReadMoreText, readLessText: kReadLessText, font: nil, charatersBeforeReadMore: kCharacterBeforReadMore, activeLinkColor: UIColor.blue, isReadMoreTapped: readMore, isReadLessTapped: false)
    }
    func readLess(readLess: Bool) {
        gameDescLabel.showTextOnTTTAttributeLable(str: strFull, readMoreText: kReadMoreText, readLessText: kReadLessText, font: UIFont.init(name: "Helvetica-Bold", size: 24.0)!, charatersBeforeReadMore: kCharacterBeforReadMore, activeLinkColor: UIColor.blue, isReadMoreTapped: readLess, isReadLessTapped: true)
    }
    
    func removeHtmlTags(with description : String) -> String {
        return description.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    func getSymbol(forCurrencyCode code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        if locale.displayName(forKey: .currencySymbol, value: code) == code {
            let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: code)
        }
        return locale.displayName(forKey: .currencySymbol, value: code)
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
    }
    
    @IBAction func Click_close_img(_ sender: UIButton) {
        
        bg_imgView.isHidden = true
        img_big.image = nil
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAllTrophiesSegue" {
            if let vc = segue.destination as? TrophiesViewController {
                vc.gameDetails = gameDetails
            }
        }
    }
}

extension GamesDetailsViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 1 {
            
            if gameDetails?.trophies.count == 0 {
                return 1
            }
            
            return 6
        }
        else {
            if gameDetails == nil {
                
                return 0
            }
            else {
                
                return (gameDetails?.medias.count)!
            }
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = gameDetails
        
        if collectionView.tag == 1 {
            
            if data?.trophies.count == 0 {
                if let cell = trophyCollectionView.dequeueReusableCell(withReuseIdentifier: "noTrophyCell", for: indexPath) as? NoTrophiesCollectionViewCell {
                    return cell
                }
            } else {
                if let cell = trophyCollectionView.dequeueReusableCell(withReuseIdentifier: "trophyCell", for: indexPath) as? TrophyCollectionViewCell {
                    if indexPath.row == 5 {
                        cell.trophyImageView.isHidden = true
                        let totalTrophies = data?.trophies.count ?? 0
                        let viewAllTrophies = totalTrophies - 5
                        cell.trophyNameLabel.text = "+ \(viewAllTrophies) View All"
                    } else {
                        cell.trophyImageView.isHidden = false
                        cell.trophyImageView.sd_setImage(with: URL(string: data?.trophies[indexPath.row].covers.service_url ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: [], context: nil)
                        cell.trophyNameLabel.text = data?.trophies[indexPath.row].name
                    }
                    return cell
                }
            }
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mediacell", for: indexPath)
            let media = data?.medias[indexPath.row]
            
            let imageView : UIImageView = UIImageView()
            imageView.frame = CGRect(x: 0, y: 0, width: ((collectionView.vWidth-10)/3), height: ((collectionView.vWidth-10)/3))
            
            if media?.remote_type == "preview" {
                
                let url = URL(string: (media?.remote_url)!)

                if let thumbnailImage = getThumbnailImage(forUrl: url!) {
                        imageView.image = thumbnailImage
                    }
                let playimg = UIImageView(frame: CGRect(x: (imageView.vWidth - 40)/2, y: (imageView.vWidth - 40)/2, width: 40, height: 40))
                playimg.image = #imageLiteral(resourceName: "play-button.png")
                imageView.addSubview(playimg)
            }
            else {
                imageView.sd_setImage(with: URL(string: (media?.remote_url)!), completed: nil)
                
            }
            
            
            cell.addSubview(imageView)
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
}

extension GamesDetailsViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 1 {
            
            if indexPath.row == 5 {
                performSegue(withIdentifier: "showAllTrophiesSegue", sender: self)
            }
        }
        else {
            
            let data = gameDetails
            let media = data?.medias[indexPath.row]
            if media?.remote_type == "preview" {
                
                let videoURL = URL(string: (media?.remote_url)!)
                let player = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
            else {
                
                bg_imgView.isHidden = false
                img_big.sd_setImage(with: URL(string: (media?.remote_url)!), completed: nil)
                
            }
        }

    }
}

extension GamesDetailsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 1 {
            
            if gameDetails?.trophies.count ?? 0 > 0 {
                return CGSize(width: (self.trophyCollectionView.frame.size.width / 3) - 5 , height: 150)
            }
            return CGSize(width: (self.trophyCollectionView.frame.size.width) , height: 60)
        }
        else {
            return CGSize(width: (collectionView.vWidth-10)/3, height: (collectionView.vWidth-10)/3)
        }
        
    }
}

extension GamesDetailsViewController: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWithTransitInformation components: [AnyHashable : Any]!) {
        if let _ = components as? [String: String] {
            if let value = components["ReadMore"] as? String, value == "1" {
                self.readMore(readMore: true)
            }
            if let value = components["ReadLess"] as? String, value == "1" {
                self.readLess(readLess: true)
            }
        }
    }
}
