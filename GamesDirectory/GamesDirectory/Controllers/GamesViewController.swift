//
//  ViewController.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 11/02/2021.
//

import UIKit
import SDWebImage

class GamesViewController: UIViewController,UISearchBarDelegate {
    
    var arrGames = [GameData]()
    var pageIndex = 1
    var isResult = false
    var isSearch = false
    let searchBar = UISearchBar()
    var closeBtn = UIBarButtonItem()
    var searchBtn = UIBarButtonItem()
    var timer = Timer()
    var gameSlugToPass = ""
    var gameNameToPass = ""
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Games"
        getGamesData()
        
        
        searchBar.sizeToFit()
        searchBar.placeholder = ""
        searchBar.delegate = self
        
        searchBtn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchClick(sender:)))
        closeBtn = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(closeSearch(sender:)))
        
        navigationItem.rightBarButtonItem = searchBtn
    }
    
    func getGamesData() {
        
        if NetworkConnection.shared.isConnectedToNetwork() {
            self.hudProggess(view, Show: true)
            
            var url = Constants.Services.GAMELIST_API_URL + "\(pageIndex)"
            
            if isSearch {
                
                let urlString = searchBar.text!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

                url = url + "&query=\(urlString!)"
            }
            
            GameService.shared.getGames(url: url)
            GameService.shared.completionHandler { (gameDatas, status, message) in
                self.hudProggess(self.view, Show: false)
                
                if status {
                    self.isResult = true
                    
                    guard let gameDatas = gameDatas else {
                        print("gameData not found")
                        return
                    }
                    
                    for obj in gameDatas.games {
                        self.arrGames.append(obj)
                    }
                    
                    self.collectionView.reloadData()
                }
            }
        } else {
            print("network connection error")
        }
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == collectionView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                if NetworkConnection.shared.isConnectedToNetwork() {
                    if isResult {
                        pageIndex = pageIndex + 1
                        isResult = false
                        hudProggess(self.view, Show: true)
                        
                        var url = Constants.Services.GAMELIST_API_URL + "\(pageIndex)"
                        if isSearch {
                            guard let searchText = searchBar.text else {
                                print("no search text")
                                return
                            }
                            let urlString = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            url = url + "&query=\(urlString!)"
                        }
                        GameService.shared.getGames(url: url)
                        GameService.shared.completionHandler { (gameDatas, status, message) in
                            self.hudProggess(self.view, Show: false)
                            
                            if status {
                                self.isResult = true
                                guard let gameDatas = gameDatas else {
                                    print("gameData not found")
                                    return
                                }
                                
                                for obj in gameDatas.games {
                                    self.arrGames.append(obj)
                                }
                                
                                self.collectionView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchData(sender:)), userInfo: nil, repeats: false)
           
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func searchData(sender:Timer) {
        
        arrGames.removeAll()
        collectionView.reloadData()
        pageIndex = 1
        isSearch = true
        getGamesData()
    }
    
    @objc func searchClick(sender:UIBarButtonItem) {
        
        self.navigationController?.navigationBar.topItem?.titleView = searchBar
        searchBar.becomeFirstResponder()
        navigationItem.rightBarButtonItem = closeBtn
    }
    
    @objc func closeSearch(sender:UIBarButtonItem) {
        arrGames.removeAll()
        collectionView.reloadData()
        isSearch = false
        self.navigationController?.navigationBar.topItem?.titleView = nil
        self.title = "Games"
        searchBar.text = ""
        navigationItem.rightBarButtonItem = searchBtn
        pageIndex = 1
        getGamesData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGamesDetailSegue" {
            if let vc = segue.destination as? GamesDetailsViewController {
                vc.gameName = gameNameToPass
                vc.gameSlug = gameSlugToPass
            }
        }
    }
}

extension GamesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        gameNameToPass = arrGames[indexPath.row].name
        gameSlugToPass = arrGames[indexPath.row].slug
        performSegue(withIdentifier: "showGamesDetailSegue", sender: self)
    }
}

extension GamesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrGames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell {
            cell.g_img.layer.cornerRadius = 10
            
            let data = arrGames[indexPath.row]
            cell.name_lbl.text = data.name
            if data.covers.service_url != "" {
                
                cell.g_img.sd_setImage(with: URL(string: data.covers.service_url), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: [], context: nil)
            }
            
            var txt = ""
            if data.platforms != nil {
                
                for obj in data.platforms {

                    if txt == "" {
                        txt = "\(obj)"
                    }
                    else {
                        txt = txt + ", \(obj)"
                    }
                }
                
                cell.g_platform.text = txt
            }
            
           
            return cell
        }
        
        return UICollectionViewCell()
    }
}

extension GamesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView.frame.size.width / 2) - 5 , height: 300)
    }
}

