//
//  TrophiesViewController.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 19/02/2021.
//

import UIKit

class TrophiesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var gameDetails : GameDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(TrophyCollectionViewCell.nib(),
                                forCellWithReuseIdentifier: "trophyCell")
    }
}

extension TrophiesViewController: UICollectionViewDelegate {
    
}

extension TrophiesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gameDetails?.trophies.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = gameDetails
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "trophyCell", for: indexPath) as? TrophyCollectionViewCell {
            
            cell.trophyImageView.sd_setImage(with: URL(string: data?.trophies[indexPath.row].covers.service_url ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), options: [], context: nil)
            cell.trophyNameLabel.textAlignment = .center
            cell.trophyNameLabel.text = data?.trophies[indexPath.row].name
            
            return cell
        }
        return UICollectionViewCell()
    }
}

extension TrophiesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: (self.collectionView.frame.size.width / 3) - 5 , height: 200)
    }
}
