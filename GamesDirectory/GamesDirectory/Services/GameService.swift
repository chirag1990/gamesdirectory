//
//  GameService.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 13/02/2021.
//

import Foundation
import Alamofire

class GameService {
    
    static let shared = GameService()
    typealias GameCallBack = (_ games: GamesModel?, _ status: Bool, _ message: String) -> Void
    var callBack : GameCallBack?
    
    func getGames(url:String) {
        
        print("getUrl:",url)
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            guard let data = responseData.data else {
                self.callBack?(nil, false, "")
                return
            }
            do {
                let gamedata = try JSONDecoder().decode(GamesModel.self, from: data)
                self.callBack?(gamedata, true, "")
            } catch {
                print(error)
                self.callBack?(nil, false, error.localizedDescription)
            }
        }
    }
    
    func completionHandler(callBack: @escaping GameCallBack) {
        self.callBack = callBack
    }
    
}
