//
//  GameDetailsService.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 18/02/2021.
//

import Foundation
import Alamofire

class GameDetailsService {
    
    static let shared = GameDetailsService()
    typealias GameDetailsCallBack = (_ games: GamesDetailsModel?, _ status: Bool, _ message: String) -> Void
    var callBack : GameDetailsCallBack?
    
    func getGamesDetails(url:String) {
        print("getUrl:",url)
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil).response { (responseData) in
            guard let data = responseData.data else {
                self.callBack?(nil, false, "")
                return
            }
            do {
                let gamedata = try JSONDecoder().decode(GamesDetailsModel.self, from: data)
                self.callBack?(gamedata, true, "")
            } catch {
                print(error)
                self.callBack?(nil, false, error.localizedDescription)
            }
        }
    }
    
    func completionHandler(callBack: @escaping GameDetailsCallBack) {
        self.callBack = callBack
    }
}
