//
//  Extensions.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 19/02/2021.
//

import UIKit
import TTTAttributedLabel

extension StringProtocol {
    var firstUppercased: String { prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}

extension UIView {
    
    var vHeight : CGFloat {
        
        get { return self.frame.size.height }
        set { self.frame.size.height = newValue }
    }
    
    var vWidth : CGFloat {
        
        get { return self.frame.size.width }
        set { self.frame.size.width = newValue }
    }
    
}

extension TTTAttributedLabel {
    func showTextOnTTTAttributeLable(str: String, readMoreText: String, readLessText: String, font: UIFont?, charatersBeforeReadMore: Int, activeLinkColor: UIColor, isReadMoreTapped: Bool, isReadLessTapped: Bool) {
        
        let text = str + readLessText
        let attributedFullText = NSMutableAttributedString.init(string: text)
        let rangeLess = NSString(string: text).range(of: readLessText, options: String.CompareOptions.caseInsensitive)
        //Swift 5
        // attributedFullText.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.blue], range: rangeLess)
        attributedFullText.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.blue], range: rangeLess)
        
        var subStringWithReadMore = ""
        if text.count > charatersBeforeReadMore {
            
            let start = String.Index(encodedOffset: 0)
            let end = String.Index(encodedOffset: charatersBeforeReadMore)
            subStringWithReadMore = String(text[start..<end]) + readMoreText
        }
        
        let attributedLessText = NSMutableAttributedString.init(string: subStringWithReadMore)
        let nsRange = NSString(string: subStringWithReadMore).range(of: readMoreText, options: String.CompareOptions.caseInsensitive)
        //Swift 5
        // attributedLessText.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.blue], range: nsRange)
        attributedLessText.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.blue], range: nsRange)
        //  if let _ = font {// set font to attributes
        //   self.font = font
        //  }
        self.attributedText = attributedLessText
        self.activeLinkAttributes = [NSAttributedString.Key.foregroundColor : UIColor.blue]
        //Swift 5
        // self.linkAttributes = [NSAttributedStringKey.foregroundColor : UIColor.blue]
        self.linkAttributes = [NSAttributedString.Key.foregroundColor : UIColor.blue]
        self.addLink(toTransitInformation: ["ReadMore":"1"], with: nsRange)
        
        if isReadMoreTapped {
            self.numberOfLines = 0
            self.attributedText = attributedFullText
            self.addLink(toTransitInformation: ["ReadLess": "1"], with: rangeLess)
        }
        if isReadLessTapped {
            self.numberOfLines = 0
            self.attributedText = attributedLessText
        }
    }
}

