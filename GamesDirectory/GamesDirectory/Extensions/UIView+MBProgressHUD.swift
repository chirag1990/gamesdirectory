//
//  UIView+MBProgressHUD.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 13/02/2021.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    
    func hudProggess(_ view:UIView,Show:Bool){
        if Show {
            let hud = MBProgressHUD.showAdded(to: view, animated: true)
            hud.contentColor = UIColor.black
            hud.label.text = "Loading"
        }
        else{
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
}
