//
//  GamesDetailsModel.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 18/02/2021.
//

import Foundation

struct GamesDetailsModel: Codable {
    let game : GameDetails
}

struct GameDetails : Codable {
    let name, gameDescription : String
    let covers : Covers
    let platforms: [String]
    let publishers : [Publishers]
    let skus : [Skus]
    let genres : [Genres]
    let trophies : [Trophies]
    let releaseDate : String
    let medias : [Medias]
    
    enum CodingKeys: String, CodingKey {
        case name
        case gameDescription = "description"
        case covers
        case platforms
        case publishers
        case skus
        case genres
        case trophies
        case releaseDate = "release_date"
        case medias
    }
}

struct Covers: Codable {
    let service_url: String
    enum CodingKeys: String, CodingKey {
        case service_url
    }
}

struct Publishers: Codable {
    let label : String
    
    enum CodingKeys: String, CodingKey {
        case label
    }
}

struct Skus: Codable {
    let price : Int
    let currency : String
    
    enum CodingKeys: String, CodingKey {
        case price = "price_cents"
        case currency = "price_currency"
    }
}

struct Genres: Codable {
    let value : String
    
    enum CodingKeys: String, CodingKey {
       case value
    }
}

struct Trophies: Codable {
    let name : String
    let covers : Covers
    
    enum CodingKeys: String, CodingKey {
        case name
        case covers
    }
}

struct Medias: Codable {
    
    let remote_type: String?
    let remote_url: String?
    
    enum CodingKeys: String, CodingKey {
        
        case remote_type
        case remote_url
    }
}
