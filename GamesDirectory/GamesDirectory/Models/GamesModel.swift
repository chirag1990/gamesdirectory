//
//  GamesModel.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 13/02/2021.
//

import Foundation

struct GamesModel: Codable {
    
    let games: [GameData]
}

struct GameData: Codable {
    
    let platforms: [String]
    let name : String
    let covers : Covers
    let slug : String
    
    
    enum CodingKeys: String, CodingKey {
        
        case platforms
        case name
        case covers
        case slug
        
    }
    
    struct Covers: Codable {
        
        let service_url: String
        
        enum CodingKeys: String, CodingKey {
            
            case service_url
        }
    }
    

}
