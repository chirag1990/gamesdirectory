//
//  Constants.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 13/02/2021.
//

import Foundation

public enum Constants {
    
    public enum Services {
        public static let BASE_URL = "https://games.directory/api/v1/play_station/games"
        public static let GAMELIST_API_URL = "\(BASE_URL)?page="
    }
    
}
