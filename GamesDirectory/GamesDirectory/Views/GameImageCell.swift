//
//  GameImageCell.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 13/02/2021.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var g_platform: UILabel!
    @IBOutlet weak var g_img: UIImageView!
    
}
