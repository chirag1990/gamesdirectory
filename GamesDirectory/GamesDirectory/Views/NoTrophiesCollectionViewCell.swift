//
//  NoTrophiesCollectionViewCell.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 19/02/2021.
//

import UIKit

class NoTrophiesCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "NoTrophiesCollectionViewCell", bundle: nil)
    }

}
