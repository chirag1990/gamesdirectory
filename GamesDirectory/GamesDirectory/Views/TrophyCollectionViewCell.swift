//
//  TrophyCollectionViewCell.swift
//  GamesDirectory
//
//  Created by Chirag Tailor on 19/02/2021.
//

import UIKit

class TrophyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var trophyImageView: UIImageView!
    @IBOutlet weak var trophyNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "TrophyCollectionViewCell", bundle: nil)
    }

}
